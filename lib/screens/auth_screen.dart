import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';

import '../widgets/auth/auth_form.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  State createState() {
    return _AuthScreenState();
  }
}

class _AuthScreenState extends State<AuthScreen> {
  var _isLoading = false;

  void _showLoading(bool show) {
    if (mounted) {
      setState(() {
        _isLoading = show;
      });
    }
  }

  Future<void> _onAuthenticate(String email, String? userName, String password,
      File? image, bool isLogin) async {
    try {
      _showLoading(true);
      final auth = FirebaseAuth.instance;
      UserCredential userCredential;
      if (isLogin) {
        userCredential = await auth.signInWithEmailAndPassword(
            email: email, password: password);
      } else {
        userCredential = await auth.createUserWithEmailAndPassword(
            email: email, password: password);
        final imageUrl = await _saveUserImage(userCredential, image!);
        await _saveUserInfo(userCredential, userName, imageUrl);
      }
      _showLoading(false);
    } catch (e) {
      _showError(e.toString());
    }
  }

  Future<String> _saveUserImage(
      UserCredential userCredential, File file) async {
    final ref = FirebaseStorage.instance
        .ref()
        .child('user_image')
        .child('${userCredential.user?.uid}.jpg');
    await ref.putFile(file);
    return await ref.getDownloadURL();
  }

  Future<void> _saveUserInfo(
      UserCredential userCredential, String? userName, String imageUrl) async {
    await FirebaseFirestore.instance
        .collection('users')
        .doc(userCredential.user?.uid)
        .set({
      'email': userCredential.user?.email,
      'userName': userName,
      'imageUrl': imageUrl,
    });
  }

  void _showError(String message) {
    print(message);
    _showLoading(false);
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(message),
        backgroundColor: Theme.of(context).colorScheme.error));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: Stack(
        children: [
          AuthForm(_onAuthenticate),
          Visibility(
              visible: _isLoading,
              child: Positioned.fill(
                child: Container(
                  color: Colors.white54,
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
