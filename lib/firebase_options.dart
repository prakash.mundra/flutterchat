// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyBpSx0r4GsmU8K_-35Vt2FFJmhqOhEsZ0w',
    appId: '1:573551764906:web:1d0179038d162dcd7af7d3',
    messagingSenderId: '573551764906',
    projectId: 'chat-42811',
    authDomain: 'chat-42811.firebaseapp.com',
    storageBucket: 'chat-42811.appspot.com',
    measurementId: 'G-MPQDZQ4GHQ',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyDt0VjPiDnhM0eXr5DnYij9wwpSDEax_xg',
    appId: '1:573551764906:android:efc3076ee5a852627af7d3',
    messagingSenderId: '573551764906',
    projectId: 'chat-42811',
    storageBucket: 'chat-42811.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyA7klOet8H3slrsV14iYLV6luRCzkvH_dc',
    appId: '1:573551764906:ios:9951bf6a6d2582207af7d3',
    messagingSenderId: '573551764906',
    projectId: 'chat-42811',
    storageBucket: 'chat-42811.appspot.com',
    iosClientId:
        '573551764906-e2g06eo8d0rh7t8rerluc5roa8u6hsv4.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterChat',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyA7klOet8H3slrsV14iYLV6luRCzkvH_dc',
    appId: '1:573551764906:ios:9951bf6a6d2582207af7d3',
    messagingSenderId: '573551764906',
    projectId: 'chat-42811',
    storageBucket: 'chat-42811.appspot.com',
    iosClientId:
        '573551764906-e2g06eo8d0rh7t8rerluc5roa8u6hsv4.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterChat',
  );
}
