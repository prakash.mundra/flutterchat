import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class NewMessage extends StatefulWidget {
  const NewMessage({super.key});

  @override
  State createState() {
    return _NewMessageState();
  }
}

class _NewMessageState extends State<NewMessage> {
  final _controller = TextEditingController();
  var _message = '';

  void _onSend() async {
    FocusScope.of(context).unfocus();
    final message = _message;
    _controller.clear();
    setState(() {
      _message = '';
    });
    final currentUser = FirebaseAuth.instance.currentUser;
    final userDoc = await FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser?.uid)
        .get();
    await FirebaseFirestore.instance.collection('chat').add({
      'text': message,
      'createdAt': Timestamp.now(),
      'userId': currentUser?.uid,
      'userName': userDoc['userName'],
      'imageUrl': userDoc['imageUrl'],
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              autocorrect: true,
              textCapitalization: TextCapitalization.none,
              enableSuggestions: true,
              controller: _controller,
              decoration: const InputDecoration(labelText: 'Send Message...'),
              onChanged: (value) {
                setState(() {
                  _message = value.trim();
                });
              },
            ),
          ),
          CircleAvatar(
            backgroundColor: Theme.of(context).colorScheme.primary,
            child: IconButton(
              onPressed: _message.isEmpty ? null : _onSend,
              icon: Icon(
                Icons.send,
                color: _message.isEmpty ? Colors.white38 : Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
