import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'message_bubble.dart';

class Messages extends StatelessWidget {
  final currentUser = FirebaseAuth.instance.currentUser;

  Messages({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('chat')
          .orderBy('createdAt', descending: true)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        final docs = snapshot.data.docs;
        if (docs.length == 0) {
          return const Center(
            child: Text('There are no messages yet!'),
          );
        }
        return ListView.builder(
          reverse: true,
          itemBuilder: (bContext, index) {
            final isMe = (currentUser?.uid == docs[index]['userId']);
            return MessageBubble(
              docs[index]['text'],
              docs[index]['userName'],
              docs[index]['imageUrl'],
              isMe,
              key: ValueKey(docs[index].reference.id),
            );
          },
          itemCount: docs.length,
        );
      },
    );
  }
}
