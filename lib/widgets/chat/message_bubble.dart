import 'package:flutter/material.dart';

class MessageBubble extends StatelessWidget {
  final String message;
  final String userName;
  final String imageUrl;
  final bool isMe;

  const MessageBubble(this.message, this.userName, this.imageUrl, this.isMe,
      {super.key});

  Widget _userAvatar(ThemeData theme) {
    return CircleAvatar(
      radius: 21,
      backgroundColor: theme.colorScheme.background,
      child: CircleAvatar(
        radius: 20,
        backgroundImage: NetworkImage(imageUrl),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final theme = Theme.of(context);
    final messageStyle = TextStyle(
      color: isMe ? Colors.white : Colors.black,
      fontSize: 15,
    );
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      child: Row(
        mainAxisAlignment:
            isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          Visibility(
            visible: !isMe,
            child: _userAvatar(theme),
          ),
          Card(
            elevation: 1,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: const Radius.circular(12),
                topRight: const Radius.circular(12),
                bottomLeft:
                    isMe ? const Radius.circular(12) : const Radius.circular(0),
                bottomRight:
                    isMe ? const Radius.circular(0) : const Radius.circular(12),
              ),
            ),
            color: isMe ? theme.colorScheme.background : Colors.grey[200],
            margin: const EdgeInsets.symmetric(horizontal: 6),
            child: Container(
              constraints: BoxConstraints(maxWidth: width * 0.75),
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
              child: Column(
                crossAxisAlignment:
                    isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                children: [
                  Text(
                    userName,
                    style: messageStyle.copyWith(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 5),
                  Text(
                    message,
                    style: messageStyle,
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: isMe,
            child: _userAvatar(theme),
          ),
        ],
      ),
    );
  }
}
