import 'dart:io';

import 'package:flutter/material.dart';

import '../pickers/user_image.dart';

enum AuthMode { login, signup }

class AuthForm extends StatefulWidget {
  final Function(String email, String? userName, String password, File? image,
      bool isLogin) onAuthenticate;

  const AuthForm(this.onAuthenticate, {super.key});

  @override
  State createState() {
    return _AuthFormState();
  }
}

class _AuthFormState extends State<AuthForm> {
  final _formKey = GlobalKey<FormState>();
  var _authMode = AuthMode.login;
  String? _email, _userName, _password;
  File? _image;

  void _onImageUpload(File image) {
    _image = image;
  }

  void _onLogin() {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState != null) {
      if (_authMode == AuthMode.signup && _image == null) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Please upload image.')),
        );
        return;
      }
      final isValid = _formKey.currentState!.validate();
      if (isValid) {
        _formKey.currentState!.save();
        widget.onAuthenticate(_email!, _userName, _password!, _image,
            _authMode == AuthMode.login);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: const EdgeInsets.all(20),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Visibility(
                  visible: _authMode == AuthMode.signup,
                  child: UserImagePicker(_onImageUpload),
                ),
                TextFormField(
                  key: const ValueKey('email'),
                  autocorrect: false,
                  textCapitalization: TextCapitalization.none,
                  enableSuggestions: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                  ),
                  validator: (value) {
                    if (value!.isEmpty || !isEmailValid(value)) {
                      return 'Please enter a valid email.';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _email = value!.trim();
                  },
                ),
                Visibility(
                  visible: _authMode == AuthMode.signup,
                  child: TextFormField(
                    key: const ValueKey('username'),
                    autocorrect: true,
                    textCapitalization: TextCapitalization.words,
                    enableSuggestions: false,
                    decoration: const InputDecoration(
                      labelText: 'Username',
                    ),
                    validator: (value) {
                      if (_authMode == AuthMode.signup &&
                          (value!.isEmpty || value.length < 4)) {
                        return 'Please enter at least 4 characters for username.';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      _userName = value!.trim();
                    },
                  ),
                ),
                TextFormField(
                  key: const ValueKey('password'),
                  decoration: const InputDecoration(
                    labelText: 'Password',
                  ),
                  obscureText: true,
                  validator: (value) {
                    if (value!.isEmpty || value.length < 6) {
                      return 'Please enter at least 6 characters for password.';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _password = value!.trim();
                  },
                ),
                const SizedBox(height: 16),
                ElevatedButton(
                  onPressed: _onLogin,
                  child: Text(_authMode == AuthMode.login ? 'Login' : 'SignUp'),
                ),
                TextButton(
                  onPressed: () {
                    setState(() {
                      _authMode = _authMode == AuthMode.login
                          ? AuthMode.signup
                          : AuthMode.login;
                    });
                  },
                  child: Text(_authMode == AuthMode.login
                      ? 'Create New Account'
                      : 'I already have an account'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

bool isEmailValid(String email) {
  return RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(email);
}
