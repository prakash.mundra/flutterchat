import 'dart:io';

import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;

class UserImagePicker extends StatefulWidget {
  final Function(File image) onImageUpload;

  const UserImagePicker(this.onImageUpload, {super.key});

  @override
  State createState() {
    return _UserImagePickerState();
  }
}

class _UserImagePickerState extends State<UserImagePicker> {
  final ImagePicker _picker = ImagePicker();
  File? _image;

  void _onProfilePic() {
    if (Platform.isMacOS) {
      _openFileSelection();
    } else if (Platform.isAndroid || Platform.isIOS) {
      _onShowOptions();
    }
  }

  void _onShowOptions() {
    final theme = Theme.of(context);
    showModalBottomSheet(
        context: context,
        builder: (bContext) {
          return SafeArea(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                InkWell(
                  onTap: _onPickImage,
                  child: ListTile(
                    leading: Icon(
                      Icons.photo_album,
                      color: theme.colorScheme.primary,
                    ),
                    title: const Text('Gallery'),
                  ),
                ),
                InkWell(
                  onTap: _onTakePicture,
                  child: ListTile(
                    leading: Icon(
                      Icons.camera,
                      color: theme.colorScheme.primary,
                    ),
                    title: const Text('Camera'),
                  ),
                ),
              ],
            ),
          );
        });
  }

  void _onTakePicture() async {
    Navigator.of(context).pop();
    final XFile? xFile = await _picker.pickImage(
        source: ImageSource.camera,
        maxWidth: 150,
        maxHeight: 150,
        imageQuality: 50);
    if (xFile != null) {
      _onPickOrTakeImage(xFile);
    }
  }

  void _onPickImage() async {
    Navigator.of(context).pop();
    final XFile? xFile = await _picker.pickImage(
        source: ImageSource.gallery,
        maxWidth: 150,
        maxHeight: 150,
        imageQuality: 50);
    if (xFile != null) {
      _onPickOrTakeImage(xFile);
    }
  }

  void _openFileSelection() async {
    const XTypeGroup typeGroup = XTypeGroup(
      label: 'images',
      extensions: <String>['jpg', 'jpeg', 'png'],
    );
    final XFile? xFile =
        await openFile(acceptedTypeGroups: <XTypeGroup>[typeGroup]);
    if (xFile != null) {
      _resizeFile(xFile);
    }
  }

  void _resizeFile(XFile xFile) async {
    final file = File(xFile.path);
    final dImage = decodeImage(file.readAsBytesSync());
    if (dImage != null) {
      final rImage = copyResize(dImage, width: 150, height: 150);
      final rFile = File(path.basename(file.path));
      rFile.writeAsBytesSync(encodePng(rImage));
      setState(() {
        _image = rFile;
      });
      widget.onImageUpload(rFile);
    }
  }

  void _onPickOrTakeImage(XFile xFile) async {
    final file = File(xFile.path);
    setState(() {
      _image = file;
    });
    widget.onImageUpload(file);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return InkWell(
      customBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100),
      ),
      onTap: _onProfilePic,
      child: Padding(
        padding: const EdgeInsets.all(5),
        child: CircleAvatar(
          radius: 40,
          backgroundColor: theme.colorScheme.primary,
          backgroundImage: _image != null ? FileImage(_image!) : null,
          child: _image == null
              ? const Icon(
                  Icons.person,
                  size: 40,
                  color: Colors.white,
                )
              : null,
        ),
      ),
    );
  }
}
